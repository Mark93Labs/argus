docker service rm argus
docker build -t argus .
docker service create --name argus --network argus-net --publish 25565:25565 --constraint node.role==manager --mount type=bind,source=/var/run/docker.sock,target=/var/run/docker.sock argus

