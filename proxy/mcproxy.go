package proxy

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/swarm"
	"github.com/docker/docker/client"
)

//MCProxy Stores and proxies TCP requests
type MCProxy struct {
	IPAddress string
	Port      int16
}

//MCPacket A MC Protocol packet
type MCPacket struct {
	Length     int
	Identifier int
	Data       []byte
}

//MCServer A MC Server
type MCServer struct {
	HostName string
	IPAddr   string
	Port     uint16
	Region   string
	Service  swarm.Service
}

//Start starts the proxy
func (p *MCProxy) Start(callback func()) {
	ln, err := net.Listen("tcp", ":25565")

	if err != nil {
		log.Fatal(err)
	}

	errs := make(chan error)
	go p.listen(ln, errs)

	callback()

	for err := range errs {
		log.Println(err)
	}
}

func (p *MCProxy) listen(ln net.Listener, errs chan error) {
	for {
		conn, err := ln.Accept()
		log.Printf("New connection from %s\n", conn.RemoteAddr().String())
		if err != nil {
			errs <- err
		}
		remoteConn := p.handleFirstContact(conn)
		p.beginForwarding(conn, remoteConn, errs)
	}
}

func (p *MCProxy) handleFirstContact(conn net.Conn) net.Conn {
	var remoteConn net.Conn

	s := make([]byte, 256)

	totalRead, _ := conn.Read(s)
	fmt.Println(totalRead, s)

	packets := []MCPacket{}

	lIdx := 0
	iIdx := 1
	dIdx := 2

	for i := 0; i < totalRead; i++ {
		length := int(s[lIdx])

		if i == 0 {
			packets = append(packets, MCPacket{Length: int(s[lIdx]), Identifier: int(s[iIdx]), Data: s[dIdx:int(s[lIdx]+1)]})
		} else {
			packets = append(packets, MCPacket{
				Length:     int(s[lIdx]),
				Identifier: int(s[iIdx]),
				Data:       s[dIdx : lIdx+length+1],
			})
		}

		i += int(s[lIdx] + 1)
		lIdx = i
		iIdx = lIdx + 1
		dIdx = lIdx + 2
	}

	pc := packets[0]
	hostName := string(pc.Data[3 : len(pc.Data)-3])

	mcServer, err := p.checkServices(hostName)
	if err != nil {
		fmt.Println(err)
		return remoteConn
	}

	remoteConn, err = net.Dial("tcp", mcServer.IPAddr+":25565")

	if err != nil {
		fmt.Println(err)
		return remoteConn
	}

	n, _ := remoteConn.Write(s[:totalRead])
	fmt.Printf("Total copied - %d\n", n)

	return remoteConn
}

func (p *MCProxy) checkServices(hostName string) (MCServer, error) {
	ctx := context.Background()
	cli, err := client.NewEnvClient()

	if err != nil {
		return MCServer{}, err
	}

	s, err := cli.ServiceList(ctx, types.ServiceListOptions{})
	n, err := cli.NodeList(ctx, types.NodeListOptions{})

	if err != nil {
		return MCServer{}, err
	}

	fmt.Printf("Hostname to search:%s, length:%d\n", hostName, len(hostName))

	for _, service := range s {
		hn := service.Spec.Labels["host"]
		fmt.Printf("Service host:%s, length:%d\n", hn, len(hn))
		if strings.Compare(hn, hostName) != 0 {
			continue
		}

		fmt.Printf("New service with hostname %s found\n", hostName)

		region := service.Spec.Labels["region"]
		ipAddr := strings.Split(service.Endpoint.VirtualIPs[0].Addr, "/")[0]
		return MCServer{HostName: hostName, Service: service, Region: region, Port: 25565, IPAddr: ipAddr}, nil
	}

	return MCServer{}, fmt.Errorf("No service found")
}

func (p *MCProxy) beginForwarding(clientConn net.Conn, remoteConn net.Conn, errs chan error) {
	p.beginClientForwarding(clientConn, remoteConn, errs)
	p.beginRemoteForwarding(clientConn, remoteConn, errs)
}

func (p *MCProxy) beginClientForwarding(clientConn net.Conn, remoteConn net.Conn, errs chan error) {
	requestBuf := new(bytes.Buffer)

	go func() {
		tee := io.TeeReader(clientConn, requestBuf)
		w, err := io.Copy(remoteConn, tee)
		fmt.Printf("Client -> Remote bytes written: %d", w)
		if err != nil {
			errs <- err
		}
	}()
}

func (p *MCProxy) beginRemoteForwarding(clientConn net.Conn, remoteConn net.Conn, errs chan error) {
	responseBuf := new(bytes.Buffer)

	go func() {
		tee := io.TeeReader(remoteConn, responseBuf)
		w, err := io.Copy(clientConn, tee)
		fmt.Printf("Remote -> Client bytes written: %d", w)
		if err != nil {
			errs <- err
		}
	}()
}
