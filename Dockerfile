FROM golang:alpine as builder

RUN apk update
RUN apk add g++ make git

COPY . /go/src/argus
WORKDIR /go/src/argus

RUN go get -v .

RUN go build -o /go/bin/argus

FROM alpine:latest

RUN apk update
RUN apk add ca-certificates
RUN update-ca-certificates

COPY --from=builder /go/bin/argus /usr/bin/argus

EXPOSE 25565

CMD ["argus"]