package main

import (
	"fmt"
	"log"
	"os"

	"github.com/argus/proxy"
	"github.com/gin-gonic/gin"
)

func main() {
	os.Setenv("DOCKER_API_VERSION", "1.39")

	go func() {
		router := gin.Default()
		router.GET("/", func(c *gin.Context) {
			c.Status(200)
		})

		err := router.Run(":4242")

		if err != nil {
			fmt.Println(err)
		}
	}()

	mcProxy := proxy.MCProxy{}

	mcProxy.Start(func() {
		log.Println("🚀 Minecraft proxy started 🚀")
	})
}
