Argus is a Proof of Concept for a TCP Reverse Proxy for Minecraft severs running in either Docker Swarm or Kubernetes. It currently supports what is effectively DNS for Minecraft servers that sit within a cluster of containers. It does this by interrogating the TCP traffic and pulling out the Hostname of the server and then looking for a match in the swarm of containers.

After a connection is established it then begins to forward the traffic between client and server over a socket managed in a Golang channel.

The reverse proxy does work locally and if you have the correct setup you can do this over the internet. 

A fun project that required deep diving in to the Minecraft handshake protocol.
